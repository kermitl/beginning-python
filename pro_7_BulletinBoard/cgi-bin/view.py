#!/usr/bin/python3
# -*- coding:utf-8 -*-
# @Project          :beginning-python
# @FileName         :view.py
# @IDE              :PyCharm     
# @Time             :2022/6/3 10:44
# @Author           :Kermit Lee
# ==============================================
import cgi
import cgitb
import sqlite3
import sys

cgitb.enable()

print('Content-type: text/html\n')

conn = sqlite3.connect('board.sqlite')       # 没有设置账户密码
curs = conn.cursor()

form = cgi.FieldStorage()
id = form.getvalue('id')

print("""
<html>
    <head>
        <title>View Message</title>
    </head>
    <body>
        <h1>View Message</h1>
""")

try: id = int(id)
except:
    print('Invalid message ID')
    sys.exit()

curs.execute("SELECT * FROM messages WHERE id = %s" % id)
# rows = curs.fictfetchall()        # 在MYSQL和PostgreSQL中使用
names = [d[0] for d in curs.description]
rows = [dict(zip(names, row)) for row in curs.fetchall()]

if not rows:
    print('Unknown message ID')
    sys.exit()

row = rows[0]

print("""
        <p><b>Subject:</b> {subject} <br />
        <b>Sender:</b> {sender}<br />
        <pre>{text}</pre>
        </p>
        <hr />
        <a href='main.py'>Back to the main page</a>
        | <a href='edit.py?reply_to={id}'>Reply</a>
    </body>
</html>
""".format_map(row))


# if __name__ == '__main__':
#     pass
