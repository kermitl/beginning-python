#!/usr/bin/python3
# -*- coding:utf-8 -*-
# @Project          :beginning-python
# @FileName         :edit.py
# @IDE              :PyCharm     
# @Time             :2022/6/3 10:44
# @Author           :Kermit Lee
# ==============================================

import cgi
import cgitb
import sqlite3
import sys

cgitb.enable()

print('Content-type: text/html\n')

conn = sqlite3.connect('board.sqlite')       # 没有设置账户密码
curs = conn.cursor()

form = cgi.FieldStorage()
reply_to = form.getvalue('reply_to')

print("""
<html>
    <head>
        <title>Compose Message</title>
    </head>
    <body>
        <h1>Compose Message</h1>
        
        <form action='save.py' method='POST'>
""")

subject = ''
if reply_to is not None:
    print('<input type="hidden" name="reply_to" value="{}"/>'.format(reply_to))
    curs.execute("SELECT subject FROM messages WHERE id = %d" % int(reply_to))
    subject = curs.fetchone()[0]
    if not subject.startswith('Re: '):
        subject = 'Re: ' + subject

print("""
        <b>Subject:</b><br />
        <input type='text' size='40' name='subject' value='{}' /><br />
        <b>Sender:</b><br />
        <input type='text' size='40' name='sender' /><br />
        <b>Message:</b><br />
        <textarea name='text' cols='40' rows='20'></textarea><br />
        <input type='submit' value='Save'/>
        </form>
        <hr />
        <a href='main.py'>Back to the main page</a>
    </body>
</html>
""".format(subject))

# if __name__ == '__main__':
#     pass
