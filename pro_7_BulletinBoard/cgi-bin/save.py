#!/usr/bin/python3
# -*- coding:utf-8 -*-
# @Project          :beginning-python
# @FileName         :save.py
# @IDE              :PyCharm     
# @Time             :2022/6/3 11:11
# @Author           :Kermit Lee
# ==============================================

import cgi
import cgitb
import sqlite3
import sys

cgitb.enable()

print('Content-type: text/html\n')

conn = sqlite3.connect('board.sqlite')       # 没有设置账户密码
curs = conn.cursor()

form = cgi.FieldStorage()

sender = form.getvalue('sender')
subject = form.getvalue('subject')
text = form.getvalue('text')
reply_to = form.getvalue('reply_to')

if not (sender and subject and text):
    print('Please supply sender, subject, and text')
    sys.exit()

if reply_to is not None:
    query = """INSERT INTO messages(reply_to, sender, subject, text) VALUES(%s, '%s', '%s', '%s')""" % (int(reply_to), sender, subject, text)
else:
    query = """INSERT INTO messages(sender, subject, text) VALUES('%s', '%s', '%s')""" % (sender, subject, text)

curs.execute(query)
conn.commit()

print("""
<html>
    <head>
        <title>Message Saved</title>
    </head>
    <body>
        <h1>Message Saved</h1>
        <hr />
        <a href='main.py'>Back to the main page</a>
    </body>
</html>
""")

# if __name__ == '__main__':
#     pass
