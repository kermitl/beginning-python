#!/usr/bin/python3
# -*- coding:utf-8 -*-
# @Project          :beginning-python
# @FileName         :main.py
# @IDE              :PyCharm     
# @Time             :2022/6/2 11:03
# @Author           :Kermit Lee
# ==============================================

import cgitb
import sqlite3

cgitb.enable()

conn = sqlite3.connect('board.sqlite')       # 没有设置账户密码
curs = conn.cursor()

print("""
<html>
    <head>
        <title>The FooBar Bullrtin Board</title>
    </head>
        <body>
            <h1>The FooBar Bulltin Board</h1>
""")

curs.execute('SELECT * FROM messages')
# rows = curs.fictfetchall()        # 在MYSQL和PostgreSQL中使用
names = [d[0] for d in curs.description]
rows = [dict(zip(names, row)) for row in curs.fetchall()]

toplevel = []
children = {}

for row in rows:
    parent_id = row['reply_to']
    if parent_id is None:
        toplevel.append(row)
    else:
        children.setdefault(parent_id, []).append(row)


def format(row):
    print('<p><a href="view.py?id={id}">{subject}</a></p>'.format_map(row))
    try: kids = children[row['id']]
    except KeyError: pass
    else:
        print('<blockquote>')
        for kid in kids:
            format(kid)
        print('</blockquote>')
    print('<p>')


for row in toplevel:
    format(row)


print("""
    </p>
        <hr />
        <p><a href="edit.py">Post message</a></p>
    </body>
</html>
""")
