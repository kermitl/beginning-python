#!/usr/bin/env python3
# -*- coding:utf-8 -*-
# @Project          :beginning-python
# @FileName         :newagent2.py
# @IDE              :PyCharm     
# @Time             :2022/5/31 16:51
# @Author           :Kermit Lee
# ==============================================

from nntplib import NNTP, decode_header
from urllib.request import urlopen
import textwrap
import re


class NewsAgent:
    """
    可以从新闻来源获取新闻项目并且发布到新闻目标的对象。
    """

    def __init__(self):
        self.sources = []
        self.destinations = []

    def add_source(self, source):
        self.sources.append(source)

    def addDestination(self, dest):
        self.destinations.append(dest)

    def distribute(self):
        """
        从所有来源中获取所有新闻并且发布到所有的项目。
        :return:
        """
        items = []
        for source in self.sources:
            items.extend(source.get_items())
        for dest in self.destinations:
            dest.receive_items(items)


class NewsItem:
    """
    包括标题和主体文本的简单新闻项目。
    """
    def __init__(self, title, body):
        self.title = title
        self.body = body


class NNTPSource:
    """
    从NNTP组中获取新闻项目的新闻来源。
    """
    def __init__(self, servername, group, howmany):
        self.servername = servername
        self.group = group
        self.howmany = howmany

    def get_items(self):
        server = NNTP(self.servername)
        resp, count, first, last, name = server.group(self.group)
        start = last - self.howmany + 1
        resp, overviews = server.over((start, last))
        for id, over in overviews:
            title = decode_header(over['subject'])
            resp, info = server.body(id)
            body = '\n'.join(line.decode('latin') for line in info.lines) + '\n\n'
            yield NewsItem(title, body)
        server.quit()


class SimpleWebSource:
    """
    使用正则表达式从网页中提取新闻项目的新闻来源。
    """

    def __init__(self, url, title_pattern, body_pattern, encoding='utf-8'):
        self.url = url
        self.title_pattern = re.compile(title_pattern)
        self.body_pattern = re.compile(body_pattern)
        self.encoding = encoding

    def get_items(self):
        text = urlopen(self.url).read().decode(self.encoding)
        titles = self.title_pattern.findall(text)
        bodies = self.body_pattern.findall(text)
        for title, body in zip(titles, bodies):
            yield NewsItem(title, textwrap.fill(body) + '\n')


class PlainDestination:
    """
    将所有新闻项目格式化为纯文本的新闻目标类
    """

    def receive_items(self, items):
        for item in items:
            print(item.title)
            print('-' * len(item.title))
            print(item.body)


class HTMLDestination:
    """
    将所有的新闻项目格式化为HTML的项目表。
    """

    def __init__(self, filename):
        self.filename = filename

    def receive_items(self, items):
        out = open(self.filename, 'w')
        print("""
        <html>
            <head>
                <title>Today's News</title>
            </head>
        <body>
        <h1>Today's News</h1>
        """, file=out)

        print('<ul>', file=out)
        id = 0
        for item in items:
            id += 1
            print(' <li><a href="#{}">{}</a></li>'.format(id, item.title), file=out)
        print('</ul>', file=out)

        id = 0
        for item in items:
            id += 1
            print('<h2><a name="{}">{}</a></h2>'.format(id, item.title), file=out)
            print('<pre>{}</pre>'.format(item.body), file=out)

        print("""
        </body>
        </html>
        """, file=out)


def runDefaultSetup():
    """
    来源和目标的默认位置。可以自己修改。
    :return:
    """

    agent = NewsAgent()

    # # A SimpleWebSource that retrieves news form Reuters:
    # reuters_url = 'http://www.reuters.com/news/world'
    # reuters_title = r'<h2><a href="[^"]*"\s*>.(.*?)</a>'
    # reuters_body = r'</h2><p>(.*?)</p>'
    # reuters = SimpleWebSource(reuters_url, reuters_title, reuters_body)
    #
    # agent.add_source(reuters)

    # An NNTPSource that retrieves news form comp.lang.python.announce:
    # clpa_server = 'news.foo.bar'    # Insert real server name
    clpa_server = 'news.gmane.io'
    clpa_group = 'gmane.comp.python.committers'
    clpa_howmany = 10
    clpa = NNTPSource(clpa_server, clpa_group, clpa_howmany)

    agent.add_source(clpa)

    # Add plain-text destination and an HTML destination:
    agent.addDestination(PlainDestination())
    agent.addDestination(HTMLDestination('news.html'))

    # Distribute the news items:
    agent.distribute()


if __name__ == '__main__':
    runDefaultSetup()
