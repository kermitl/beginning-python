#!/usr/bin python3

import cgi
import sys
import os

print('Conten-type: text/html\n')


BASE_DIR = os.path.abspath('pro_6_RemoteEditing_with_CGI/data')
# print("This is edit.cgi\n")
form = cgi.FieldStorage()
filename = form.getvalue('filename')
if not filename:
    print('Please enter a file name')
    sys.exit()
filepath = os.path.join(BASE_DIR, filename)

print(filepath)
text = open(filepath).read()

print("""
<html>
    <head>
        <title>Editing...</title>
    </head>
    <body>
        <form action='save.cgi' method='filename' />
            <b>File:</b> {} <br />
            <input type='hidden' value='{}' name='filename' />
            <b>Password:</b><br />
            <input name='password' type='password' /><br />
            <b>Text:</b><br />
            <textarea name='text' cols='40' rows='20'>{}</textarea><br />
            <input type='submit' calue='Save' />
        </form>
    </body>
</html>
""".format(filename, filename, text))
