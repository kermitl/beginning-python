# -*- coding:utf-8 -*-
# @Project          :beginning-python
# @FileName         :save.py
# @IDE              :PyCharm     
# @Time             :2022/6/1 21:01
# @Author           :Kermit Lee
# ==============================================

# import codecs


from os.path import join
from os.path import abspath
from hashlib import sha1
import cgi
import sys
import html
from urllib import parse

# sys.stdout = codecs.getwriter('utf8')(sys.stdout.buffer)
print('Content-type: text/html\n')

BASE_DIR = abspath('data')

form = cgi.FieldStorage()

text = form.getvalue('text')
# 编码问题！！！
text = html.unescape(parse.unquote(text))
# text = parse.unquote(text)
filename = form.getvalue('filename')
password = form.getvalue('password')

if not (filename and text and password):
    print('Invalid parameters.')
    sys.exit()

if sha1(password.encode()).hexdigest() != '8843d7f92416211de9ebb963ff4ce28125932878':
    print('Invalid password')
    sys.exit()

f = open(join(BASE_DIR, filename), 'w', encoding='utf-8')
f.write(text)
f.close()

print('The file has been saved.')

