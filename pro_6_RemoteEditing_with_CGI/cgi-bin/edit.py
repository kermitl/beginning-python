#!/usr/bin/env python3
# -*- coding:utf-8 -*-
# @Project          :beginning-python
# @FileName         :edit.py
# @IDE              :PyCharm     
# @Time             :2022/6/1 20:40
# @Author           :Kermit Lee
# ==============================================
import cgi
import sys
import os

print('Conten-type: text/html\n')

BASE_DIR = os.path.abspath('data')
# print(BASE_DIR)
# print("This is edit.cgi\n")
form = cgi.FieldStorage()
filename = form.getvalue('filename')
if not filename:
    print('Please enter a file name')
    sys.exit()
filepath = os.path.join(BASE_DIR, filename)

# print(filepath)
text = open(filepath).read()

print("""
<html>
    <head>
        <title>Editing...</title>
    </head>
    <body>
        <form action='save.py' method='filename' />
            <b>File:</b> {} <br />
            <input type='hidden' value='{}' name='filename' />
            <b>Password:</b><br />
            <input name='password' type='password' /><br />
            <b>Text:</b><br />
            <textarea name='text' cols='40' rows='20'>{}</textarea><br />
            <input type='submit' calue='Save' />
        </form>
    </body>
</html>
""".format(filename, filename, text))
