#!/usr/bin/python3
# -*- coding:utf-8 -*-
# @Project          :beginning-python
# @FileName         :config.py
# @IDE              :PyCharm     
# @Time             :2022/6/5 16:48
# @Author           :Kermit Lee
# ==============================================

# Configuration file for Squish
# Suqish 的配置文件
# -----------------------------

# Feel free to modify the configuration variables below to taste.
# If the game is too fast or too slow, try to modify the speed
# variables.
# 按照自己的喜好配置修改下面的变量即可
# 如果游戏太慢或者太快，可以修改速度变量
# 改变这些设置可以改变在游戏中使用的图像。

banana_image = 'banana.png'
weight_image = '10-ton.png'
splash_image = '10-ton.png'

# 改变这些设置会改变游戏的界面外观
screen_size = 1200, 600
background_color = 255, 255, 255
margin = 30
full_screen = 0
font_size = 48

# 这些设置会影响游戏的物体移动速度和状态
drop_speed = 1
banana_speed = 10
speed_increase = 1
weights_pre_level = 10
banana_pad_top = 40
banana_pad_side = 20

# if __name__ == '__main__':
#     pass
