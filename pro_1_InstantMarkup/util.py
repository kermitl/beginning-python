#!/usr/bin/env python3
# -*- coding:utf-8 -*-
# @Project          :beginning-python
# @FileName         :util.py
# @IDE              :PyCharm     
# @Time             :2022/5/29 16:42
# @Author           :Kermit Lee
# ==============================================

def lines(file):
    for line in file:
        yield line
    yield '\n'


def blocks(file):
    """
    对文本分段
    :param file: 要处理的文件
    :return:     一段一段返回
    """
    block = []
    for line in lines(file):
        if line.strip():
            block.append(line)
        elif block:
            yield ''.join(block).strip()
            block = []


# if __name__ == '__main__':
#     pass
