#!/usr/bin/env python3
# -*- coding:utf-8 -*-
# @Project          :beginning-python
# @FileName         :simple_markup.py
# @IDE              :PyCharm     
# @Time             :2022/5/29 17:12
# @Author           :Kermit Lee
# ==============================================
import re
import sys

from util import *

print('<html><head><title>...</title><body>')

title = True
for block in blocks(sys.stdin):
    block = re.sub(r'\*(.+?)\*', r'<em>\1</em>', block)
    # print(block)
    if title:
        print('<h1>')
        print(block)
        print('</h1>')
        title = False
    else:
        print('<p>')
        print(block)
        print('</p>')

print('</body></html>')
