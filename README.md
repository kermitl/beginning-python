# Beginning-Python

#### 介绍
Beginning Python - From Novice to Professional

本仓库包含十个小项目，均来自于《Python基础教程》（第2版·修订版）中后半部分的
十个项目，其中也参考了该书的英文版第三版《Beginning Python - From Novice to Professional》


#### 内容介绍
1. 项目1 即时标记 （中文版 书里是这么翻译的，下同）
2. 项目2 画幅好画
3. 项目3 万能的XML
4. 项目4 新闻聚合
5. 项目5 虚拟茶话会
6. 项目6 使用CGI进行远程编辑
7. 项目7 自定义电子公告板
8. 项目8 使用XML-RPC进行文件共享
9. 项目9 文件共享2----GUI版本
10. 项目10 DIY街机游戏


#### 一些说明

1.  所有项目代码均已通过测试
2.  测试系统为win10
3.  python版本为3.7

#### 最后

感谢本书带入进一步入门python
希望整理过的代码可以帮到Python的初学者
